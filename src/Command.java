import java.io.IOException;

/**
 *
 * @author Facedown
 */
public interface Command {
    public void act(final String argument) throws IOException;
}
