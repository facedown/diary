import java.io.IOException;

/**
 *
 * @author Facedown
 */
final public class TurnCommand implements Command {
    final private Diary diary;
    
    public TurnCommand(final Diary diary) {
        this.diary = diary;        
    }
    
    public void act(final String argument) throws IOException {
        Page currentPage;
        if(argument.isEmpty())
            currentPage = this.diary.turnPage();
        else
            currentPage = this.diary.turnPage(Integer.parseInt(argument));
        System.out.printf("Strana %d\r\n", currentPage.number());
    }
}
