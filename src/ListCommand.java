import java.io.IOException;

/**
 *
 * @author Facedown
 */
final public class ListCommand implements Command {
    final private Diary diary;
	
    public ListCommand(final Diary diary) {
            this.diary = diary;        
    }
	
    public void act(final String argument) throws IOException {
        System.out.println(this.diary.list());
    }
}
