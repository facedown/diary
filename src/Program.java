import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Toto je moje úplně první aplikace napsaná v Javě, PHP ftw :)
 * Nevím, jak správně pracovat s datumem, proto jsem ho vynechal jako kriterium pro hledání
 * @author Facedown
 */
public class Program {
    public static void main(String[] args) {
        try {
            Scanner input = new Scanner(System.in, "UTF-8");
            System.out.println("Zadej počet řádků na stránku");
            final Integer rows = Integer.parseInt(input.nextLine());
            System.out.println("Zadej počet stran v deníčku");
            final Integer pageNumbers = Integer.parseInt(input.nextLine());
            Diary diary = new Diary(insertedPages(pageNumbers, rows));
            System.out.println("Super, vytvořil sis deník, teď ho jen naplnit");
            command(input, diary);
        } catch(IOException ex) {
            System.out.println(ex.getMessage());
        } catch(Exception ex) {
            System.out.println("Vypadá to, že se nepodařil vytvořit deník");
        }
    }
    
    public static void command(
        final Scanner input,
        final Diary diary
    ) throws IOException {
        Boolean quit = false;
        final Map<String, Command> allowedCommands = new HashMap(){{
            put("turn", new TurnCommand(diary));
            put("read", new ReadCommand(diary));
            put("write", new WriteCommand(diary));
            put("list", new ListCommand(diary));
            put("quit", new DumpCommand());
        }};
        System.out.printf(
            "Pro ovládání použij příkazy %s\r\n",
            String.join(", ", allowedCommands.keySet())
        );
        try {
            while(quit == false) {
                final List<String> commands = new ArrayList(
                    Arrays.asList(input.nextLine().split(" "))
                );
                final String name = commands.get(0).toLowerCase();
                commands.remove(0);
                final String argument = String.join(" ", commands);
                if(name.equals("quit"))
                    quit = true;
                else if(!allowedCommands.containsKey(name))
                    throw new InterruptedException(
                        String.format(
                            "Povolené příkazy jsou pouze %s",
                            String.join(", ", allowedCommands.keySet())
                        )
                    );
                allowedCommands.get(name).act(argument);
            }
        } catch(IOException ex) {
            System.out.println(ex.getMessage());
            command(input, diary);
        } catch(InterruptedException ex) {
            System.out.println(ex.getMessage());
            command(input, diary);
        } catch(NumberFormatException ex) {
            System.out.println("Vypadá to, že si zadal špatný formát");
            command(input, diary);
        }
    }
    
    public static List<Page> insertedPages(
        final Integer pageNumbers,
        final Integer rows
    ) throws IOException {
        List<Page> pages = new ArrayList(pageNumbers + 1);
        pages = withFrontPage(pages);
        for(Integer pageNumber = 1; pageNumber <= pageNumbers; pageNumber++)
            pages.add(new Page(pageNumber, rows));
        return pages;
    }
    
    public static List<Page> withFrontPage(final List<Page> pages) throws IOException {
        pages.add(
            new Page(0).write(new FormattedRecord("Dominikův deník"))
        );
        return pages;
    }
}