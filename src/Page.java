import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Má přehled co na ní je napsané
 * Nezná jiné strany, zná jen sama sebe
 * Ví jak se vypsat
 * Může na ní být zapsáno pouze v podobě záznamu
 * @author Facedown
 */
final public class Page {
    final private Integer rows;
    final private Integer number;
    final private List<Record> records;
    
    public Page(final Integer number) {
        this(number, 10);
    }
    
    public Page(final Integer number, final Integer rows) {
        this.number = number;
        this.rows = rows;
        this.records = new ArrayList(this.rows);
    }
    
    public String read() {
        StringBuilder appendedRecords = new StringBuilder();
        appendedRecords.append(String.format("%d.Strana\r\n", this.number()));
        for(Record record : this.records)
            appendedRecords.append(record.toString());
        return appendedRecords.toString();
    }
    
    public Page write(final Record record) throws IOException {
        if(this.records.size() >= this.rows || this.rows <= 0) {
            throw new IOException(
                String.format(
                    "Stránka %d je již zaplněná",
                    this.number
                )
            );
        }
        this.records.add(record);
        return this;
    }
    
    public Integer number() {
        return this.number;
    }
    
    public Boolean equals(final Integer number) {
        return this.number.equals(number);
    }
}
