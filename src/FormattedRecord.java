import java.util.Calendar;


/**
 * Formátuje výstup do srozumitelné a pěkné podoby
 * @author Facedown
 */
final public class FormattedRecord extends Record {
    public FormattedRecord(final String text) {
        super(text);
    }
    
    public FormattedRecord(final Calendar date, final String text) {
        super(date, text);
    }
    
    public String toString() {
        return String.format(
            "%s  -  %s\r\n",
            this.toCzechDate(this.date()),
            this.toPrettyText(this.text())
        );
    }
    
    private String toCzechDate(final Calendar date) {        
        return String.format(
            "%d.%d.%d %d:%d",
            date.get(Calendar.DAY_OF_MONTH),
            date.get(Calendar.MONTH) + 1,
            date.get(Calendar.YEAR),
            date.get(Calendar.HOUR_OF_DAY),
            date.get(Calendar.MINUTE)
        );
    }
    
    private String toPrettyText(final String record) {
        if(record.isEmpty())
            return "Není tu žádný záznam :(";
        return String.format("|%s|", record);
    }
}
