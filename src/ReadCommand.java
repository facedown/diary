import java.io.IOException;

/**
 *
 * @author Facedown
 */
final public class ReadCommand implements Command {
    final private Diary diary;
    
    public ReadCommand(final Diary diary) {
        this.diary = diary;        
    }
    
    public void act(final String argument) throws IOException {
        System.out.println(this.diary.read());
    }
}
