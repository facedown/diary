import java.io.IOException;
import java.util.List;


/**
 * Dokáže pracovat se všemi strany
 * Má přehled o všech stranách
 * Několik stran = deník
 * @author Facedown
 */
final public class Diary {
    final private List<Page> pages;
    private Page currentPage;
    
    public Diary(final List<Page> pages) throws IOException {
        this.pages = pages;
        this.currentPage = this.firstPage();
    }
    
    public Page turnPage(final Integer number) throws IOException {
    	for(Page page : this.pages)
            if(page.equals(number))
                return this.currentPage = page;
        throw new IOException(String.format("Strana %d neexistuje", number));
    }
    
    public Page turnPage() throws IOException {
        return this.turnPage(this.currentPage.number() + 1);
    }
    
    public String read() throws IOException {
        return this.currentPage.read();
    }
    
    public String list() throws IOException {  
        StringBuilder appendedRecords = new StringBuilder();
        for(Page page : this.pages)
            appendedRecords.append(page.read());
        return appendedRecords.toString();
    }
    
    public void write(final Record record) throws IOException {
        this.currentPage.write(record);
    }
    
    private Page firstPage() throws IOException {
        if(this.pages.isEmpty())
            throw new IOException("Diář bez stran není diář - doplň strany");
        return this.pages.get((this.pages.size() - this.pages.size()) + 1);
    }
}
