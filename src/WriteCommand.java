import java.io.IOException;

/**
 *
 * @author Facedown
 */
final public class WriteCommand implements Command {
    final private Diary diary;
	
    public WriteCommand(final Diary diary) {
        this.diary = diary;        
    }
	
    public void act(final String argument) throws IOException {
        this.diary.write(new FormattedRecord(argument));
    }
}
