import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Záznam, který se skládá z datumu a z textu
 * @author Facedown
 */
public abstract class Record {
    private final Calendar date;
    private final String text;
    
    public Record(final String text) {
        this(new GregorianCalendar(), text);
    }
    
    public Record(final Calendar date, final String text) {
        this.date = date;
        this.text = text;
    }
    
    final public Calendar date() {
        return this.date;
    }    
    
    final public String text() {
        return this.text;
    }
    
    abstract public String toString();
}
